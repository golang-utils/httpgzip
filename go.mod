module gitlab.com/golang-utils/httpgzip

go 1.19

require (
	golang.org/x/net v0.22.0
	golang.org/x/tools v0.19.0
)

require golang.org/x/text v0.14.0 // indirect
